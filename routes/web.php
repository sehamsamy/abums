<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/login', function () {
//    return view('admin.layout.login');
//});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::group(array('prefix' => '/', 'as' => 'website.', 'namespace' => 'website'), function () {

    Route::get('/', 'indexController@index')->name('index');
    Route::get('/singleAlbum/{id}', 'indexController@singleAlbum')->name('singleAlbum');
    Route::get('/myAlbums/{id}', 'indexController@myAlbums')->name('myAlbums');
    Route::get('/profile/{id}', 'indexController@profile')->name('profile');

    Route::post('/update-profile/{id}', 'indexController@UpdateProfile')->name('UpdateProfile');
    Route::post('/addAlbum', 'indexController@addAlbum')->name('addAlbum');

    Route::delete('/deleteAlbum/{id}', 'indexController@deleteAlbum')->name('deleteAlbum');
    Route::get('/Album/{id}', 'indexController@editAlbum')->name('editAlbum');
    Route::post('/update/{id}', 'indexController@update')->name('update');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(array('prefix' => 'dashboard','as'=>'admin.' ,'middleware' => ['auth','admin'], 'namespace' => 'admin'), function() {

    Route::get('/', 'indexController@index')->name('layout.main');
    Route::resource('users', 'userController');
    Route::resource('albums', 'albumController');
    Route::resource('roles','roleController');

});
