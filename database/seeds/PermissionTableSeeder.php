<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            'عرض المهام',
            'اضافة مهمه',
            'تعديل مهمه',
            'حذف مهمة',
            'عرض الالبومات',
            'اضافة البوم',
            'تعديل البوم',
            'حذف البوم'

        ];

        foreach ($permissions as $permission) {

            Permission::create(['name' => $permission]);

        }

    }
}
