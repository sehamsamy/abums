
@extends('layouts.master')
@section('styles')
@stop
@section('content')

    <!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url({{asset('website/img/bg-img/breadcumb.jpg')}});">
        <div class="bradcumbContent">

        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->



    <!-- ##### Contact Area Start ##### -->
    <section class="contact-area section-padding-0-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <p>See what’s new</p>
                        <h2>My Profile</h2>
                    </div>
                </div>
            </div>

            <div class="row profile" >
                <div class="col-12">
                    <!-- Contact Form Area -->
                    <div class="contact-form-area">
                        {!!Form::model(auth()->user() , ['route' => ['website.UpdateProfile' , auth()->id()] ,'class'=>'form','method' => 'POST','files'=>true]) !!}
                            <div class="row">
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::text("name",null,['class'=>'form-control','id'=>'name','placeholder'=>'name'])!!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::email("email",null,['class'=>'form-control','id'=>'email','placeholder'=>'email'])!!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::text("phone",null,['class'=>'form-control','id'=>'phone','placeholder'=>'phone'])!!}

                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::password("password",['class'=>'form-control','placeholder'=>'new password'])!!}
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-6">
                                    <div class="form-group">
                                        {!! Form::password("password_confirmation",['class'=>'form-control','placeholder'=>'rewrite new password'])!!}
                                    </div>
                                </div>
                                <div class="col-12 text-center">
                                    <button class="btn oneMusic-btn mt-30" type="submit">Update <i class="fa fa-angle-double-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->
@stop