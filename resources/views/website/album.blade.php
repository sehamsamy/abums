<?php
use Carbon\Carbon;
?>

<!-- Single Event Area -->

<div class=" col-12 col-md-6 col-lg-4">
        <div class="single-event-area mb-30">
            <div class="event-thumbnail">
                <img src="{{getimg($album->image)}}" alt="">
            </div>
            <div class="event-text">
                <h4>{{$album->name}}</h4>
                <div class="event-meta-data">
                    @if ($album->status=="general")
                        <a href="#" class="event-place">Public</a>
                    @else
                        <a href="#" class="event-place">Private</a>
                    @endif
                    <a href="#" class="event-date">{{Carbon::parse($album->created_at)->toFormattedDateString()}}</a>
                </div>
                <a href="#" class="btn see-more-btn">See Event</a>
            </div>
        </div>

</div>