
@extends('layouts.master')
@section('styles')
@stop
@section('content')
    <!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url({{asset('website/img/bg-img/breadcumb3.jpg')}});">
        <div class="bradcumbContent">
            <p>See what’s new</p>
            <h2>single Album</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->
    <?php
    use Carbon\Carbon;
    ?>

    <!-- ##### Blog Area Start ##### -->
    <div class="blog-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-9">

                    <!-- Single Post Start -->
                    <div class="single-blog-post mb-100 wow fadeInUp" data-wow-delay="100ms">
                        <!-- Post Thumb -->
                        <div class="blog-post-thumb mt-30">
                            <a href="#"><img src="{{getimg($album->image)}}" alt=""></a>
                            <!-- Post Date -->
                            {{--<div class="post-date">--}}
                                {{--<span></span>--}}
                                {{--<span></span>--}}
                            {{--</div>--}}
                        </div>

                        <!-- Blog Content -->
                        <div class="blog-content">



                            <!-- ##### Latest Albums Area Start ##### -->
                            <section class="latest-albums-area section-padding-100">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="section-heading style-2">
                                                <p>{{Carbon::parse($album->created_at)->toFormattedDateString()}}</p>
                                                <h2>{{$album->name}}</h2>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-12">
                                            <div class="albums-slideshow owl-carousel">
                                                <!-- Single Album -->
                                                <?php
                                                $galleries=\App\Gallery::where('album_id',$album->id)->get();

                                                ?>
                                                @foreach($galleries as $image)
                                                <div class="single-album">

                                                    <img src="{{getimg($image->image)}}" alt="">
                                                    <div class="album-info">
                                                        <a href="#">
                                                            <h5></h5>
                                                        </a>

                                                    </div>
                                                </div>
                                               @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- ##### Latest Albums Area End ##### -->


                        </div>
                    </div>



                    {{--<!-- Pagination -->--}}
                    {{--<div class="oneMusic-pagination-area wow fadeInUp" data-wow-delay="300ms">--}}
                    {{--<nav>--}}
                    {{--<ul class="pagination">--}}
                    {{--<li class="page-item active"><a class="page-link" href="#">01</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="#">02</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="#">03</a></li>--}}
                    {{--</ul>--}}
                    {{--</nav>--}}
                    {{--</div>--}}
                </div>


            </div>
        </div>
    </div>

    <!-- ##### Blog Area End ##### -->

@stop
