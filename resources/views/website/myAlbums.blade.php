
@extends('layouts.master')
@section('styles')
@stop
@section('content')
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url({{asset('website/img/bg-img/breadcumb3.jpg')}});">
        <div class="bradcumbContent">
            <p>See what’s new</p>
            <h2> My Albums</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Events Area Start ##### -->
    <section class="events-area section-padding-100">
        <div class="container">
            <div class="row album">

                <?php
                use Carbon\Carbon;
                ?>

                <!-- Single Event Area -->
                    @foreach($allAlbums as $album)
                        <a href="{{route('website.singleAlbum',$album->id)}}">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="single-event-area mb-30">
                                <div class="event-thumbnail">
                                    <img src="{{getimg($album->image)}}" alt="">
                                </div>
                                <div class="event-text">
                                    <h4>{{$album->name}}</h4>
                                    <div class="event-meta-data">
                                        @if ($album->status=="general")
                                            <a href="#" class="event-place">Public</a>
                                        @else
                                            <a href="#" class="event-place">Private</a>
                                        @endif
                                        <a href="#" class="event-date">{{Carbon::parse($album->created_at)->toFormattedDateString()}}</a>
                                    </div>
                                    <a href="{{route("website.editAlbum",$album->id)}}" class="btn btn-primary btn-circle" > edit</a>

                                    <a href="#" class="btn btn-danger btn-circle" onclick="Delete({{$album->id}})" > delete</a>

                                    {!!Form::open( ['route' => ['website.deleteAlbum',$album->id] ,'id'=>'delete-form'.$album->id, 'method' => 'Delete']) !!}
                                    {!!Form::close() !!}
                                </div>
                            </div>
                        </div>
                        </a>
                    @endforeach



            </div>

            <div class="row">
                <div class="col-12">
                    <div class="load-more-btn text-center mt-70">
                        <a href="#" class="btn oneMusic-btn">Load More <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Events Area End ##### -->

    <!-- ##### Newsletter & Testimonials Area Start ##### -->
    <section class="newsletter-testimonials-area">
        <div class="container">
            <div class="row">

                <!-- Newsletter Area -->
                <div class="col-12 col-lg-12">
                    <div class="newsletter-area mb-100">
                        <div class="section-heading text-left mb-50">
                            <p>See what’s new</p>
                            <h2>Subscribe to newsletter</h2>
                        </div>
                        <div class="newsletter-form">
                            <form id="form">
                                @csrf
                                <input type="hidden" name="user_id" value={{Auth::user()->id}}   >
                                <input type="text" name="name" id="newsletterSearch" placeholder="AlbumName">
                                <select name="status"  class="select"  >
                                    <option>AlbumStatus</option>
                                    <option value="general">Public</option>
                                    <option value="private">Private</option>
                                </select>
                                <span>*add general image to album</span>
                                <input type="file" name="image" >
                                <span>*add multiple images to album</span>
                                <input type="file" name="images[]" multiple >

                                <button type="submit" class="btn oneMusic-btn">Add Album <i class="fa fa-angle-double-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </section>
@stop

@section('footer')
<script>

    $("#form").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var formData = new FormData(form[0]);

        $.ajax({
            type: "POST",
            url: '{{route('website.addAlbum')}}',
            data: formData,
            contentType: false,
            processData: false,

            success: function (data) {


                $('.album').append(data.album)

            }


        })

    });


    function Delete(id) {
        var item_id=id;
        console.log(item_id);
        swal({
            title: " ",
            text: "Do you  want  to delete this Album ?",
            icon: "warning",
            buttons: ["cancel", "yes"],
            dangerMode: true,

        }).then(function(isConfirm){
            if(isConfirm){
                document.getElementById('delete-form'+item_id).submit();
            }
            else{
                swal("Canceled", "deleting canceled",'info',{buttons:'yes'});
            }
        });
    }
</script>
    @stop