
@extends('layouts.master')
@section('styles')
@stop
@section('content')
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url({{asset('website/img/bg-img/breadcumb3.jpg')}});">
        <div class="bradcumbContent">
            <p>See what’s new</p>
            <h2>edit my Album</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->



    <!-- ##### Newsletter & Testimonials Area Start ##### -->
    <section class="newsletter-testimonials-area">
        <div class="container">
            <div class="row">

                <!-- Newsletter Area -->
                <div class="col-12 col-lg-12">
                    <div class="newsletter-area mb-100">
                        <div class="section-heading text-left mb-50">
                            <p>See what’s new</p>
                            <h2>Subscribe to newsletter</h2>
                        </div>
                        <div class="newsletter-form">
                            <form id="form" method="post" action="{{route('website.update',$album->id)}}">
                                @csrf
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <input type="hidden" name="user_id" value={{Auth::user()->id}}   >
                                <input type="text" name="name" id="newsletterSearch" placeholder="AlbumName" value="{{$album->name}}">
                              @if ($album->status=='general')
                                    <select name="status"  class="select" >
                                        <option>AlbumStatus</option>
                                        <option value="general" selected>Public</option>
                                        <option value="private">Private</option>
                                    </select>

                               @else
                                    <select name="status"  class="select" >
                                        <option>AlbumStatus</option>
                                        <option value="general" >Public</option>
                                        <option value="private" selected>Private</option>
                                    </select>

                                @endif

                                <span>*add general image to album</span>
                                <input type="file" name="image" >
                                <span>*add multiple images to album</span>
                                <input type="file" name="images[]" multiple >

                                <button type="submit" class="btn oneMusic-btn">Add Album <i class="fa fa-angle-double-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </section>
@stop
