
@extends('layouts.master')
@section('styles')
@stop
@section('content')

<!-- ##### Breadcumb Area Start ##### -->
<section class="breadcumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/breadcumb3.jpg);">
    <div class="bradcumbContent">
        <p>See what’s new</p>
        <h2>Login</h2>
    </div>
</section>
<!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Login Area Start ##### -->
    <section class="login-area section-padding-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <div class="login-content">
                        <h3>Welcome Back</h3>
                        <!-- Login Form -->
                        <div class="login-form">
                            <form action="{{route('register')}}" method="post">
                                @csrf

                                <div class="form-group">
                                    <label for="name">UserName</label>
                                    <input type="text" name="name" class="form-control" id="name"   placeholder="Enter Name">
                                </div>
                                <div class="form-group">
                                    <label for="phone">phone</label>
                                    <input type="text" name="phone" class="form-control" id="phone"   placeholder="Enter phone">
                                </div>

                                <div class="form-group">
                                    <label >Email address</label>
                                    <input type="email" class="form-control" name="email"  placeholder="Enter E-mail">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label for="passwordcofirm">password Confirmation</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="passwordcofirm" aria-describedby="emailHelp" placeholder="Re-enter password">
                                </div>
                                <button type="submit" class="btn oneMusic-btn mt-30">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Login Area End ##### -->

@stop