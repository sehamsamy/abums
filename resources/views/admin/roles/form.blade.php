@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<input type="hidden" name="id" value="@isset($user) {!! $user->id !!} @endisset">
<div class="col-sm-12 col-xs-12  pull-right">
    <div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>
</div>
<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <strong>الصلاحيات:</strong>
        <br/>
        @foreach($permission as $value)
            <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
          {{ Form::checkbox('permission[]', $value->id, false, array('class' => 'switchery','id'=> $value->id)) }}
                <label style="margin-left: 20px;"  for={{$value->id}}>
                {{ $value->name }}
            </label>
            </div>
        @endforeach
    </div>
</div>







<div class="col-xs-12">
    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
</div>


