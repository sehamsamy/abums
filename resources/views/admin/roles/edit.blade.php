@extends('admin.layout.app')
@section('title')
تعديل المهمة
{{ $role->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>تعديل المهمة      {{ $role->name }}</h2>
        <ul class="header-dropdown m-r--5">
          <a href="{{route('admin.roles.index')}}">    <button class="btn btn-danger">كل المهام </button></a>
        </ul>
      </div>
      <div class="body">
        {!!Form::model($role , ['route' => ['admin.roles.update' , $role->id] , 'method' => 'PATCH','files' => true]) !!}
        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <input type="hidden" name="id" value="@isset($user) {!! $user->id !!} @endisset">
        <div class="col-sm-12 col-xs-12  pull-right">
          <div class="form-group form-float">
            <label class="form-label">الإسم</label>
            <div class="form-line">
              {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-xs-12  pull-right">
          <div class="form-group form-float">
            <strong>الصلاحيات:</strong>
            <br/>
            @foreach($permission as $value)
              <div class="checkbox checkbox-left checkbox-switchery col-xs-2 pull-right">
                {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'switchery','id'=> $value->id)) }}
                <label style="margin-left: 20px;"  for={{$value->id}}>
                  {{ $value->name }}
                </label>
              </div>
            @endforeach
          </div>
        </div>







        <div class="col-xs-12">
          <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
        </div>

        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection
