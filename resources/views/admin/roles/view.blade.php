@extends('admin.layout.app')

@section('title')
  عرض  بطاقات الشحن
@endsection
@section('header')
  {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
@endsection
@section('content')
  <!-- Exportable Table -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>
            عرض  بطاقات الشحن
          </h2>
          <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.card_contracts.create')}}">   <button class="btn btn-success">إضافة بطاقات شحن جديد</button></a>
          </ul>
        </div>
        <div class="body">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
            <tr>
              <th>#</th>
              <th>العميل</th>
              <th>نوع العقد</th>

              <th> تاريخ بدء الاقساط </th>
              <th>المبلغ </th>
              <th>القيمه المضافة </th>

            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>#</th>
              <th>العميل</th>
              <th>نوع العقد</th>

              <th> تاريخ بدء الاقساط </th>
              <th>المبلغ </th>
              <th>القيمه المضافة </th>


            </tr>
            </tfoot>
            <tbody>
            @foreach($contracts as $key=>$contract)
              <tr>
                <td> {{++$key}}</td>
                <td>{{$contract->client->name}}</td>


                  @if ($contract->type=="car")
                  <td> عقد سيارات </td>
                    @else
                  <td>  عقد بطاقات شحن</td>
                  @endif



                <td>{{$contract->start_date}}</td>

                <td>{{$contract->amount}}</td>
                <td>{{$contract->added_value}}</td>

              </tr>
            @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Exportable Table -->

@endsection

@section('footer')
  <script>
    function Delete(id) {
      var item_id=id;
      console.log(item_id);
      swal({
        title: "هل أنت متأكد ",
        text: "هل تريد حذف هذا المستخدم ؟",
        icon: "warning",
        buttons: ["الغاء", "موافق"],
        dangerMode: true,

      }).then(function(isConfirm){
        if(isConfirm){
          document.getElementById('delete-form'+item_id).submit();
        }
        else{
          swal("تم االإلفاء", "حذف  المستخدم تم الغاؤه",'info',{buttons:'موافق'});
        }
      });
    }
  </script>
@endsection


@section('data-table')
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

  <script type="text/javascript"
          src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script>
@endsection