@extends('admin.layout.app')
@section('title')
تعديل الالبوم
{{ $album->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>تعديل  الالبوم     {{ $album->name }}</h2>
        <ul class="header-dropdown m-r--5">
          <a href="{{route('admin.albums.index')}}">    <button class="btn btn-danger">كل الالبوم </button></a>
        </ul>
      </div>
      <div class="body">
        {!!Form::model($album , ['route' => ['admin.albums.update' , $album->id] , 'method' => 'PATCH','files' => true]) !!}
        @include('admin.albums.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection
