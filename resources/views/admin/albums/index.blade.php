@extends('admin.layout.app')

@section('title')
  عرض  المستخدمين
@endsection
@section('header')
  {{Html::style('admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}
@endsection
@section('content')
  <!-- Exportable Table -->
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>
            عرض  المستخدمين
          </h2>
          <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.albums.create')}}">   <button class="btn btn-success">إضافة مستخدم  جديد</button></a>
          </ul>
        </div>
        <div class="body">
          <table class="table table-bordered table-striped table-hover dataTable js-exportable">
            <thead>
            <tr>
              <th>#</th>
              <th>الإسم</th>
              <th>حاله الالبوم </th>
              <th>الصورة الرئيسية </th>
              <th>العمليات</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>#</th>
              <th>الإسم</th>
              <th>حاله الالبوم </th>
              <th>الصورة الرئيسية </th>
              <th>العمليات</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($albums as $key=>$album)
              <tr>
                <td>{{++$key}}</td>
                <td>{{$album->name}}</td>

                @if ($album->status=="general")
                  <td><label class="label label-primary"> عام</label></td>
                @else
                  <td><label class="label label-success"> خاص</label></td>
                @endif

                <td><img src="{{getimg($album->image)}}" style="width:100px; height:100px"> </td>
                <td>
                  @role('مستخدم')
                  <a href="{{route('admin.albums.edit',['id'=>$album->id])}}" class="btn btn-info btn-circle"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i></a>
                   @endrole
                    <a href="#"  onclick="Delete({{$album->id}})"  data-original-title="حذف" class="btn btn-danger btn-circle"><i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i></a>
                    {!!Form::open( ['route' => ['admin.albums.destroy',$album->id] ,'id'=>'delete-form'.$album->id, 'method' => 'Delete']) !!}
                    {!!Form::close() !!}



                </td>

              </tr>
            @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Exportable Table -->

@endsection

@section('footer')
  <script>
    function Delete(id) {
      var item_id=id;
      console.log(item_id);
      swal({
        title: "هل أنت متأكد ",
        text: "هل تريد حذف هذا المستخدم ؟",
        icon: "warning",
        buttons: ["الغاء", "موافق"],
        dangerMode: true,

      }).then(function(isConfirm){
        if(isConfirm){
          document.getElementById('delete-form'+item_id).submit();
        }
        else{
          swal("تم االإلفاء", "حذف  المستخدم تم الغاؤه",'info',{buttons:'موافق'});
        }
      });
    }
  </script>
@endsection


@section('data-table')
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.18/r-2.2.2/datatables.min.js"></script>

  <script type="text/javascript"
          src="{{asset('admin/js/plugins/tables/datatables/datatables.min.js')}}"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin/js/plugins/forms/selects/select2.min.js')}}"></script>
  <script type="text/javascript"
          src="{{asset('admin/js/plugins/datatables_extension_buttons_init.js')}}"></script>
@endsection