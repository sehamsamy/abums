@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<input type="hidden" name="id" value="@isset($user) {!! $user->id !!} @endisset">
<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>
</div>


<div class="col-sm-6 col-xs-12">
<div class="form-group form-float">
    <label class="form-label"> اسم المستخدم </label>
    <div class="form-line">
        {!! Form::select("user_id",users(),null,['class'=>'form-control js-example-basic-single ','placeholder'=>'اختاراسم  المستخدم'])!!}
    </div>
</div>
</div>


<div class="col-sm-6 col-xs-12">
    <div class="form-group form-float">
        <label >حاله الالبوم  </label>
        {!! Form::select("status",['general'=>'عام','private'=>'خاص '],null,['class'=>'form-control'])!!}
        <div class="form-line">
        </div>
    </div>
</div>




<div class="col-sm-6 col-xs-12">
    <div class="form-group form-float">
        <label>صوره الالبوم الرئيسية </label>
        {!! Form::file("image",null,['class'=>'form-control'])!!}
    </div>
</div>


@if( isset($album))
    <div class="col-sm-6 col-xs-12">
        <div class="form-group col-md-6 form-float">
            <label>الصوره السابقه: </label>
            <img src="{{getimg($album->image)}}" style="width:100px; height:100px">
        </div>
    </div>

@endif

<div class="form-group col-md-6 pull-left">
    <label>الصور </label>
    <input type="file" name="images[]" multiple>
</div>





<div class="col-xs-12">
    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
</div>


