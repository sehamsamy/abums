@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<input type="hidden" name="id" value="@isset($user) {!! $user->id !!} @endisset">
<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
  <label class="form-label">الإسم</label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
  </div>
</div>
</div>


<div class="col-sm-6 col-xs-12  pull-right">
<div class="form-group form-float">
    <label > الايميل</label>
    {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'   الايميل'])!!}
    <div class="form-line">
    </div>
</div>
</div>



<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label >الجوال</label>
        {!! Form::text("phone",null,['class'=>'form-control','placeholder'=>'الجوال  '])!!}
        <div class="form-line">
        </div>
    </div>
</div>


<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label >الرقم السرى</label>

        {!! Form::password("password",['class'=>'form-control','placeholder'=>'الرقم السرى'])!!}
        <div class="form-line">
        </div>
    </div>
</div>


<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label > تكرار  الرقم السرى </label>
        {!! Form::password("password_confirmation",['class'=>'form-control','placeholder'=>'تكرار  الرقم السرى '])!!}

        <div class="form-line">
        </div>
    </div>
</div>
@if (!isset($user))
<div class="col-sm-6 col-xs-12  pull-right">
    <div class="form-group form-float">
        <label >الدور/المهام </label>

        {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}


        <div class="form-line">
        </div>
    </div>
</div>
@else

    <div class="col-sm-6 col-xs-12  pull-right">
        <div class="form-group form-float">
            <label >الدور/المهام </label>
            {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control')) !!}


            <div class="form-line">
            </div>
        </div>
    </div>

@endif



<div class="col-xs-12">
    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
</div>


