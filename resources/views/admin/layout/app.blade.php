<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>برنامج الالبوم - @yield('title')</title>
    <!-- Favicon-->
     {{--<link rel="icon" href="{{asset('admin/images/logo.png')}}"> --}}

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <link href="{{asset('admin/hijri-calender/css/jquery.calendars.picker.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin/hijri-calender/css/hijri.css')}}" rel="stylesheet" type="text/css">
    <!-- Multi Select Css -->

      {!!Html::style('admin/plugins/multi-select/css/multi-select.css')!!}
    <!-- Bootstrap Core Css -->
   {!!Html::style('admin/plugins/bootstrap/css/bootstrap.min.css')!!}
    <!-- Waves Effect Css -->
    {!!Html::style('admin/plugins/node-waves/waves.min.css')!!}
    <!-- Animation Css -->
    {!!Html::style('admin/plugins/animate-css/animate.min.css')!!}
    <!-- Custom Css -->
    {!!Html::style('admin/css/style.min.css')!!}
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    {!!Html::style('admin/css/themes/theme-red.min.css')!!}
    {!!Html::style('admin/css/rtl.css')!!}

    {!!Html::style('admin/css/pro.css')!!}
    {!!Html::style('admin/cus/sweetalert.css')!!}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">

    <style>
    .navbar {
        background-color: #FF9800 !important;
    }
    .sidebar .user-info {
        height: 82px;
    }
    </style>

    @yield('header')
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>رجاءً انتظر...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            {{--<div class="nav-logo">--}}
                    {{--<img src="{{asset('admin/images/logo.png')}}" alt="">--}}
                {{--</div>--}}
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <form id="logout-form" action="/logout" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <a class="navbar-brand" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"> <i class="fa fa-power-off"></i> </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                {{-- <div class="image">
                <img src="{{url('admin/images/user.png')}}" width="48" height="48" alt="User" />
            </div> --}}
            <div class="info-container">
                <div class="name ar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</div>
                <div class="email ar">{{Auth::user()->email}}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-left">
                        <form id="logout-form" action="/logout" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <li><a class="ar" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" href="{{url('/logout')}}"><i class="material-icons">input</i>تسجيل الخروج</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">القائمة الرئيسية</li>
                    @include('admin.layout.nav')
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            @yield('content')
        </div>
    </section>
    <!-- Multi Select Plugin Js -->


    <!-- Jquery Core Js -->
    {!!Html::script('admin/plugins/jquery/jquery.min.js')!!}
    <!-- Bootstrap Core Js -->
    {!!Html::script('admin/plugins/bootstrap/js/bootstrap.min.js')!!}
    <!-- Select Plugin Js -->
    {!!Html::script('admin/plugins/bootstrap-select/js/bootstrap-select.min.js')!!}
    <!-- Slimscroll Plugin Js -->
    {!!Html::script('admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js')!!}
    <!-- Waves Effect Plugin Js -->
    {!!Html::script('admin/plugins/node-waves/waves.min.js')!!}
    <!-- Custom Js -->
    {!! Html::script('/admin/plugins/forms/selects/select2.min.js') !!}
    <script>

        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });

    </script>
    {!!Html::script('admin/js/admin.js')!!}
    {!!Html::script('admin/js/demo.js')!!}
    {!!Html::script('admin/cus/sweetalert.min.js')!!}
    <script type="text/javascript">
    var lis = $('.check_active');

    lis.each(function(index){
        if($(this).attr('href') === "{!!url('/')!!}" + window.location.pathname){
            $(this).parent().addClass('active');
            $(this).parent().parent().parent().addClass('active');

        }
        // console.log($(this).attr('href'));
    });
    </script>
    <script type="text/javascript">
    $(window).load(function() {
        if($( window ).width() < 481){
            $('.dt-buttons').hide();
        }
    });
    </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @include('sweet::alert')


    @if(Session::has('success'))
        <script>
            new Noty({
                type: 'info',
                layout: 'topRight',
                text: '{!! Session::get('success') !!}'
            }).show();
        </script>
    @elseif(Session::has('error'))
        <script>
            new Noty({
                type: 'error',
                layout: 'topRight',
                text: '{!! Session::get('error') !!}'
            }).show();
        </script>
    @endif

    @yield('footer')

    @yield('data-table')

@stack('scripts')



</body>
</html>
