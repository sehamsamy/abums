<li>
  <a class="check_active" href="{{route('admin.layout.main')}}">
    <i class="material-icons">home</i>
    <span>الصفحه الرئيسه والاحصائيات</span>
  </a>

</li>


  <li>
    <a href="javascript:void(0);" class="menu-toggle">
      <i class="material-icons">assignment_ind</i>
      <span>المستخدمين</span>
    </a>
    <ul class="ml-menu">
      <li >
        <a class="check_active" href="{{route('admin.users.index')}}">

          <span> عرض المستخدمين</span>
        </a>
      </li>
      <li >
        <a  class="check_active" href="{{route('admin.users.create')}}">

          <span>اضافه مستخدم جديد</span>
        </a>
      </li>
    </ul>
  </li>



<li>
  <a href="javascript:void(0);" class="menu-toggle">
    <i class="material-icons">assignment_ind</i>
    <span>الالبومات</span>
  </a>
  <ul class="ml-menu">
    <li >
      <a class="check_active" href="{{route('admin.albums.index')}}">

        <span> عرض الالبومات</span>
      </a>
    </li>
    <li >
      <a  class="check_active" href="{{route('admin.albums.create')}}">

        <span>اضافه البوم جديد</span>
      </a>
    </li>
  </ul>
</li>


<li>
  <a href="javascript:void(0);" class="menu-toggle">
    <i class="material-icons">assignment_ind</i>
    <span>المهام والصلاحيات</span>
  </a>
  <ul class="ml-menu">
    <li >
      <a class="check_active" href="{{route('admin.roles.index')}}">

        <span> عرض المهام و الصلاحيات</span>
      </a>
    </li>
    <li >
      <a  class="check_active" href="{{route('admin.roles.create')}}">

        <span>اضافه  مهام جديدة</span>
      </a>
    </li>
  </ul>
</li>
