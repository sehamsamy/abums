<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{

    protected $fillable = [
    'name','image','status','user_id'
    ];

    public function images()
    {
        return $this->hasMany(Gallery::class,'album_id');
    }



}
