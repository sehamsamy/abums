<?php

namespace App\Http\Controllers\admin;

use App\Activity;
use App\Album;
use App\City;
use App\Gallery;
use App\User;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;

class albumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.albums.index')->with('albums',Album::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.albums.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $this->validate($request,[
            'name'=>'required|string|',
            'images' => 'required|array',

            'user_id'=>'required|numeric|exists:users,id',

        ]);
        $inputs=$request->all();
        if($request->hasFile('image')) {
            $image = uploader($request->image, 'photos');
            $inputs['image']=$image;
        }

//        $inputs['user_id']=auth()->id();
        $album=Album::create($inputs);
        if ($request->hasFile('images')) {
            multiUploader($request, 'images', new Gallery(),$onId = null ,$album->id);
        }


        alert()->success('تم اضافة الالبوم بنجاح !')->autoclose(5000);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $album=Album::findOrFail($id);
        return view('admin.albums.edit',compact('album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $album=Album::findOrFail($id);
        $this->validate($request,[
            'name'=>'required|string|',
            'images' => 'sometimes|array',

            'user_id'=>'required|numeric|exists:users,id',

        ]);
        $inputs=$request->all();
        if($request->hasFile('image')) {
            $image = uploader($request->image, 'photos');
            $inputs['image']=$image;
        }

//        $inputs['user_id']=auth()->id();
        $album->update($inputs);
        if ($request->hasFile('images')) {
            $album  ->images()->delete();
            multiUploader($request, 'images', new Gallery(),$onId = null ,$album->id);
        }


        alert()->success('تم تعديل الالبوم بنجاح !')->autoclose(5000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { //dd($id);
        $album=Album::find($id);

        $album->delete();
            alert()->success('تم حذف البوم  بنجاح');
            return back();

    }
}
