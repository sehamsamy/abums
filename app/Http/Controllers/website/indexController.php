<?php

namespace App\Http\Controllers\website;

use App\Album;
use App\Banner;
use App\CartItem;
use App\Category;
use App\Contact;
use App\Contacts;
use App\Donate;
use App\Firm;
use App\Gallery;
use App\Offer;
use App\Payment;
use App\Post;
use App\Product;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class indexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $albums=Album::where('status','general')->paginate(12);
        return view('website.index',compact('albums'));
    }



    public function singleAlbum($id)
    {
        $album=Album::findOrFail($id);
        return view('website.singleAlbum',compact('album'));
    }



    public function myAlbums($id)
    {
        $allAlbums=Album::where('user_id',$id)->get();

        return view('website.myAlbums',compact('allAlbums'));
    }

    public function profile()
    {

        return view('website.profile');
    }



    public function UpdateProfile(Request $request, $id)
    {

        $user=User::find($id);
        $inputs = $request->all();

        if($request->password != null) {$user->update(['password'=>bcrypt($request->password),]);}
        $user->update(array_except($inputs,['password']));

        alert()->success('تم  تعديل البيانات بنجاح !')->autoclose(5000);
        return back();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addAlbum(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|string|',
            'images' => 'required|array',
            'image'=>'required|image',
            'user_id'=>'required|numeric|exists:users,id',

        ]);
        $inputs=$request->all();
        if($request->hasFile('image')) {
            $image = uploader($request->image, 'photos');
            $inputs['image']=$image;
        }
        $album=Album::create($inputs);
        if ($request->hasFile('images')) {
            multiUploader($request, 'images', new Gallery(),$onId = null ,$album->id);
        }

        $albums=Album::where('user_id',auth()->id())->get();


        return response()->json(['album'=>view('website.album',compact('album'))->render()]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAlbum($id)
    {
        $album=Album::find($id);

        $album->delete();
        alert()->success('Album Deleted sucessfully');
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editAlbum($id)
    {
        $album=Album::findOrFail($id);
        return view('website.editAlbum',compact('album'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $album=Album::findOrFail($id);
       // dd($request->all());
        $this->validate($request,[
            'name'=>'required|string|',
            'images' => 'sometimes|array',

            'user_id'=>'required|numeric|exists:users,id',

        ]);
        $inputs=$request->all();
     //  dd($inputs);
        if($request->hasFile('image')) {
            $image = uploader($request->image, 'photos');
            $inputs['image']=$image;
        }

//        $inputs['user_id']=auth()->id();
        $album->update($inputs);
        if ($request->hasFile('images')) {
            $album  ->images()->delete();
            multiUploader($request, 'images', new Gallery(),$onId = null ,$album->id);
        }

        alert()->success('Album edit!')->autoclose(5000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
